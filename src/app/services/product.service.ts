import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url: string = "http://localhost:8080/products";
  constructor(private http: HttpClient) {}

  getProducts():Observable<Product[]>{
    return this.http.get<Product[]>(this.url);
  }
  getProductById(id: number):Observable<any>{
    return this.http.get<Product>(this.url+'/'+id);
  }

  addProduct(product: Product): Observable<any>{
    return this.http.post(this.url, product);
  }

  updateProduct(id: number, product: Product): Observable<any>{
    return this.http.put(this.url, product);
  }

  deleteProduct(id: number): Observable<any>{
    return this.http.delete(this.url+'/'+id);
  }
  
}
