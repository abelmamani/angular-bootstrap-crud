export * from './create-product/create-product.component';
export * from './home/home.component';
export * from './navbar/navbar.component';
export * from './product-table/product-table.component';
export * from './update-product/update-product.component';
