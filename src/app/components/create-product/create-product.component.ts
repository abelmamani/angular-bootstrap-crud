import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent {
  form: FormGroup;

  constructor(
    private productService: ProductService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      id: [0, Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: [0, Validators.required],
      stock: [0, Validators.required],
      createAt: [new Date()]
    });
  }

  formSubmit() {
    if (this.form.valid) {
      this.productService.addProduct(this.form.value).subscribe({
        next: (resp) =>{
          this.router.navigate(['/']);
          Swal.fire('Confirmacion', 'Se registro el producto', 'success');
        },
        error: (err) => {
          Swal.fire('Error', err.error ? err.error.message : 'Hubo un problema al registrar', 'error');
        }
      });
    } else {
      Swal.fire('Error', 'Corrija los errores antes', 'error');
    }
  }
}
