import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models';
import { ProductService } from 'src/app/services';
import { formatearFecha } from 'src/app/utilities';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit{
  products: Product[] = [];
  constructor(private productService: ProductService){}
  
  ngOnInit(): void {
    this.getAllProducts();
  }
  
  getAllProducts(){
    this.productService.getProducts().subscribe({
      next: (resp: Product[]) =>{
      this.products = resp;
    },
    error: (err) => {
      Swal.fire("Ups!", "No se ha podido traer el listado de productos", "warning");
    }});
  }

  deleteProduct(product: Product){
    Swal.fire({
      title:"Confirmacion",
      text:"Esta seguro que dese eliminar "+product.name+" ?",
      icon:"warning",
      showCancelButton:true,
      showConfirmButton:true
    }).then((resp) => {
      if(resp.isConfirmed){
        this.productService.deleteProduct(product.id).subscribe({
          next: (resp: any) =>{
          this.getAllProducts();
        },
        error: (err) =>{
          Swal.fire("Ups!", err.error ? err.error.message : "No se ha podido eliminar el producto", "error");
        }});
      }
    });
  }

  fechaConFormato(date: Date): string{
    return formatearFecha(date);
  }

}
