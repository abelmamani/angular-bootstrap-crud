import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models';
import { ProductService } from 'src/app/services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit{
  form: FormGroup;
  producto!: Product;
  id: number = 0;
  constructor(
    private productService: ProductService,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      id: [0, Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: [0, Validators.required],
      stock: [0, Validators.required],
      createAt: [new Date()]
    });
  }
  ngOnInit(): void {
    this.activateRoute.params.subscribe((params: any) =>{
      this.id= params.id;
      this.productService.getProductById(this.id).subscribe({
        next: (resp: Product) => {
          this.form.patchValue(resp);
        },
        error: (err) =>{
          Swal.fire('Error', err.error ? err.error.message : 'Hubo un problema al obtener el producto', 'error');
        }
      });
    });
  }

  formSubmit() {
    if (this.form.valid) {
      this.productService.updateProduct(this.id, this.form.value).subscribe({
        next: (resp) => {
          this.router.navigate(['/']);
          Swal.fire('Confirmacion', 'Se actualizo el producto', 'success');
        },
        error: (err) => {
          Swal.fire('Error', err.error ? err.error.message : 'Hubo un problema al actualizar el producto', 'error');
        }});
    } else {
      Swal.fire('Error', 'Corrija los errores antes', 'error');
    }
  }
}
