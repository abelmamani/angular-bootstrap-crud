export const formatearFecha = (dateISO: Date): string => {
    const date = new Date(dateISO);
    const opcionesDeFormato: Intl.DateTimeFormatOptions = {
        year: 'numeric', 
        month: 'long', 
        day: 'numeric'
    };
    
    return date.toLocaleDateString('es-ES', opcionesDeFormato);
  }